import os


class Config:
    """Set Flask environment variables."""
    secret = os.environ.get('SECRET_KEY')
    if secret is None:
        # Generate a random secret key
        import secrets
        secret = secrets.token_hex(16)
    SECRET_KEY = secret
    FLASK_APP = os.environ.get('FLASK_APP')
    FLASK_ENV = os.environ.get('FLASK_ENV')
    FLASK_DEBUG = os.environ.get('FLASK_DEBUG', False)
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'SQLALCHEMY_DATABASE_URI', 'sqlite:////tmp/debug.sqlite3')
    SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get(
        'SQLALCHEMY_TRACK_MODIFICATIONS', False)
