function resetAmount() {
    $('#investment-amount').val(0);
    $('#update-fieldset').prop('disabled', true);
}

function updateAmount(name, amount) {
    $.ajax({
        type: 'PUT',
        url: `/api/set?key=${name}&value=${amount}`,
        success: function () {
            $('#investment-amount').val(amount);
            $('#update-fieldset').prop('disabled', false);
        },
        fail: function () {
            resetAmount();
        }
    });
}

$('#investment-create').submit(function (event) {
    event.preventDefault();
    $('#investment-create').prop('hidden', true);
    let name = $('#investment-key').val();
    updateAmount(name, 0);
});

$('#investment-update').submit(function (event) {
    event.preventDefault();
    let name = $('#investment-key').val();
    let amount = $('#investment-amount').val();
    updateAmount(name, amount);
});

$('#investment-search').submit(function (event) {
    event.preventDefault();
    let name = $('#investment-key').val();
    if (name === '') {
        // No name provided; ignore
        return;
    }
    $.get(`/api/get?key=${name}`, function (data) {
        $('#investment-create').prop('hidden', true);
        if (data.hasOwnProperty('amount')) {
            $('#investment-amount').val(data['amount']);
            $('#update-fieldset').prop('disabled', false);
        } else {
            resetAmount();
        }
    }, 'json').fail(function (data) {
        if (data.status === 404) {
            $('#investment-create').prop('hidden', false);
        } else {
            $('#investment-create').prop('hidden', true);
        }
        resetAmount();
    });
});