from flask import redirect
from flask import render_template
from flask import url_for
from flask import current_app as app
from flask_login import current_user


@app.route('/')
def home():
    """Show the investments search/update page."""
    if current_user.is_authenticated:
        return render_template('index.html', authenticated=True)
    else:
        return redirect(url_for('login'))
