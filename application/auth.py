from typing import Optional

from flask import redirect
from flask import render_template
from flask import url_for
from flask import request
from flask_login import login_required
from flask_login import logout_user
from flask_login import current_user
from flask import current_app as app
from flask_login import login_user
from werkzeug.security import generate_password_hash

from . import login_manager
from .models import db
from .models import User


@login_manager.user_loader
def load_user(user_id: str) -> Optional[User]:
    """Returns a User object, if user_id is valid, or None."""
    return User.query.get(user_id)


@app.route('/login', methods=['GET', 'POST'])
def login():
    """Present a login page for GET requests and attempt to authenticate
    the provided credentials for POST requests.
    """
    error = None
    if current_user.is_authenticated:
        # Skip credential check if user is already authenticated
        return redirect(url_for('home'))
    elif request.method == 'POST':
        # Find user in database
        username = str(request.form.get('username', ''))
        password = str(request.form.get('password', ''))
        matches = User.query.filter_by(username=username)
        user: Optional[User] = matches.first()
        if '' in (username, password):
            # Empty username or password; nothing to do
            # Don't show error but reload sign up page
            pass
        elif user is not None and user.check_password(password):
            login_user(user)
            return redirect(url_for('home'))
        else:
            # Show same error message for invalid username or invalid password
            # to avoid username enumeration attack
            error = "Invalid username or password"
    # Present login page for GET or invalid attempt
    return render_template('login.html', error=error, authenticated=False)


@app.route('/logout')
@login_required
def logout():
    """Log out the currently authenticated user."""
    logout_user()
    return redirect(url_for('home'))


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    """Present a sign up page for GET requests and create a new user account,
    with the provided credentials for POST requests.  The newly created account
    is automatically logged in.
    """
    error = None
    if request.method == 'POST':
        username = str(request.form.get('username', ''))
        password = str(request.form.get('password', ''))
        matches = User.query.filter_by(username=username)
        user: Optional[User] = matches.first()
        if '' in (username, password):
            # Empty username or password; nothing to do
            # Don't show error but reload sign up page
            pass
        elif user is None:
            password_hash = generate_password_hash(password, method='sha256')
            new = User(username=username, password=password_hash)
            db.session.add(new)
            db.session.commit()
            login_user(new)
            return redirect(url_for('home'))
        else:
            error = "Username already exists"
    # Present sign up for GET
    return render_template(
        'signup.html', error=error, authenticated=current_user.is_authenticated)
