from typing import Any
from typing import Dict
from typing import Optional

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

db = SQLAlchemy()
login_manager = LoginManager()


def create_app(config: Optional[Dict[str, Any]] = None) -> Flask:
    app = Flask(__name__, instance_relative_config=False)
    if config is None:
        app.config.from_object('config.Config')
    else:
        app.config.from_mapping(config)

    db.init_app(app)
    login_manager.init_app(app)

    with app.app_context():
        from . import routes
        from . import auth
        from . import api
        db.create_all()
        return app


if __name__ == '__main__':
    app = create_app()
    app.run(debug=True)
