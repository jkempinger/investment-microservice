from flask import abort
from flask import jsonify
from flask import request
from flask import current_app as app
from flask_login import login_required
from flask_login import current_user

from .models import db
from .models import Investment


@app.route('/api/get', methods=['GET'])
@login_required
def get():
    key = request.args.get('key')
    matches = Investment.query.filter_by(
        name=key, username=current_user.username)
    if key is None:
        # Querying an investment requires a "key"
        abort(400)
    elif matches.count() == 0:
        # Error: investment does not exist
        abort(404)
    elif matches.count() == 1:
        # Return investment as JSON
        match: Investment = matches.first()
        return jsonify({
            'name': match.name,
            'amount': match.amount,
        })
    else:
        # Error: duplicate investments found
        abort(500)


@app.route('/api/set', methods=['PUT'])
@login_required
def set():
    key = request.args.get('key')
    value = request.args.get('value')
    if None in (key, value):
        # Updating the value requires both a "key" and "value"
        abort(400)
    name: str = str(key)
    try:
        amount: int = int(str(value))
    except TypeError:
        # Invalid "value" provided
        abort(400)
    else:
        matches = Investment.query.filter_by(
            name=name, username=current_user.username)
        if matches.count() == 0:
            # Create new investment
            new = Investment(
                name=name, amount=amount, username=current_user.username)
            db.session.add(new)
            db.session.commit()
            return jsonify(success=True)
        elif matches.count() == 1:
            # Update investment
            match: Investment = matches.first()
            match.amount = amount
            db.session.commit()
            return jsonify(success=True)
        else:
            # Error: duplicate investments found
            abort(500)
