from random import randint
from secrets import token_hex

import pytest
from flask.json import loads
from werkzeug.security import generate_password_hash

from application import create_app
from application import db
from application.models import User
from application.models import Investment

test_username: str = 'testing'
test_password: str = 'password'


def fill_database(reset: bool = False):
    if reset:
        # Drop all tables and recreate to ensure no duplicate rows between tests
        db.drop_all()
        db.create_all()
    # Create a testing user
    password_hash = generate_password_hash(test_password, method='sha256')
    new = User(username=test_username, password=password_hash)
    db.session.add(new)
    # Create sample investments
    first = Investment(username=test_username, name='First', amount=1000)
    db.session.add(first)
    second = Investment(username=test_username, name='Second', amount=2000)
    db.session.add(second)
    third = Investment(username=test_username, name='Third', amount=10_000)
    db.session.add(third)
    db.session.commit()


@pytest.fixture(scope='module')
def app():
    app = create_app({
        'SECRET_KEY': token_hex(16),
        'FLASK_DEBUG': True,
        'SQLALCHEMY_DATABASE_URI': 'sqlite:///:memory:',
        'SQLALCHEMY_TRACK_MODIFICATIONS': True,
    })
    with app.app_context():
        fill_database(reset=False)
        return app


@pytest.fixture(scope='module')
def client(app):
    return app.test_client()


def login(client, username, password):
    return client.post('/login', data=dict(
        username=username,
        password=password
    ), follow_redirects=True)


def logout(client):
    return client.get('/logout', follow_redirects=True)


def test_login_logout(client):
    """Test logging in and logging out."""
    result = login(client, test_username, test_password)
    # Ensure logged in
    assert b'id="logout"' in result.data
    result = logout(client)
    # Ensure logged out
    assert b'id="login"' in result.data
    result = login(client, test_username, 'incorrect_password')
    # Ensure still logged out
    assert b'id="login"' in result.data


@pytest.mark.parametrize('username', ('admin', 'user', 'staff'))
def test_unique_signups(app, client, username):
    """Test account creation with unique usernames."""
    with app.app_context():
        before: int = User.query.count()
        _ = client.post(
            '/signup', data={'username': username, 'password': test_password})
        after: int = User.query.count()
        assert after == before + 1, (before, after)
        # Reset database after each test to ensure other tests don't attempt to
        # read usernames that are supposed to be missing
        fill_database(reset=True)


def test_duplicate_signup(app, client):
    """Test account creation with a duplicate username."""
    with app.app_context():
        before: int = User.query.count()
        _ = client.post(
            '/signup', data={'username': test_username, 'password': test_password})
        after: int = User.query.count()
        assert after == before, (before, after)


@pytest.mark.parametrize('key', ('First', 'Second', 'Third'))
def test_good_get(client, key):
    """Test getting valid keys through the API."""
    login(client, test_username, test_password)
    result = client.get(f'/api/get?key={key}')
    assert result.status_code == 200, key


@pytest.mark.parametrize('key', ('Fourth', 'Fifth', 'Sixth'))
def test_bad_get(client, key):
    """Test getting valid keys through the API."""
    login(client, test_username, test_password)
    result = client.get(f'/api/get?key={key}')
    assert result.status_code == 404, key


@pytest.mark.parametrize('key', ('First', 'Second', 'Third'))
def test_existing_set(client, key):
    result = client.get(f'/api/get?key={key}')
    data = loads(result.data)
    current = int(data['amount'])
    new = current * 10
    result = client.put(f'/api/set?key={key}&value={new}')
    assert result.status_code == 200, (key, current, new)
    result = client.get(f'/api/get?key={key}')
    data = loads(result.data)
    now = int(data['amount'])
    assert now == new, (current, new, now)


@pytest.mark.parametrize('key', ('Fourth', 'Fifth', 'Sixth'))
def test_new_set(app, client, key):
    amount = randint(10, 1_000_000)
    result = client.put(f'/api/set?key={key}&value={amount}')
    assert result.status_code == 200, key
    result = client.get(f'/api/get?key={key}')
    data = loads(result.data)
    now = int(data['amount'])
    assert now == amount, (amount, now)
    # Reset database after each test to ensure other tests don't attempt to read
    # keys that are supposed to be missing
    with app.app_context():
        fill_database(reset=True)
