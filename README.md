# Investment Microservice

* Author: Jayson Kempinger
* Created: 2019-11-19

### About

This project provides a Flask microservice for tracking investments.  New 
accounts can be created, and investments can only be accessed by the account 
that created them.  Once logged in (this will happen automatically after 
creating a new account), an investment can be searched by name.  If no 
investment has been recorded with that name, an option to create it will be 
provided.  The current investment amount (rounded to the nearest dollar) can be 
set.

### Installation

This project requires Flask, SQLAlchemy, Flask-SQLAlchemy, and Flask-Login.  
In order to run tests, pytest is required.  Please see the requirements.txt file.  
All requirements can be installed by running `pip install -r requirements.txt`.

### Running

A secret key is required, and can be set using a `SECRET_KEY` environment 
variable.

You may also wish to set a path to the SQLAlchemy database file with the 
`SQLALCHEMY_DATABASE_URI` environment variable.

The Flask server can be run with `flask run` or `python -m flask run` and the 
web application accessed at `http://127.0.0.1:5000`.